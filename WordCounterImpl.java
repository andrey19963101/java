package ru.skillbench.tasks.text;

import java.io.PrintStream;
import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;
import java.util.Map.Entry;
import java.util.Comparator;

public class WordCounterImpl implements WordCounter{
    
    public String text = null;

    @Override
    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String getText() {
        return this.text;
    }
    
    public class Pair {
        
        public String text = null;
        public int lastIndex; 

        public void setText(String text) {
            this.text = text;
        }

        public void setLastIndex(int lastIndex) {
            this.lastIndex = lastIndex;
        }

        public String getText() {
            return text;
        }

        public int getLastIndex() {
            return lastIndex;
        }
        
    }
    
    public Pair findWord(String text, int index) {
        
        Pair pair = this.new Pair();
        int beginOfWord;
        
        while (index < text.length() - 1 && text.charAt(index) == ' ' || text.charAt(index) == '\n' || text.charAt(index) == '\t' || text.charAt(index) == '<') {
            if (text.charAt(index) == '<') {
                while(text.charAt(index) != '>') {
                    index++;
                }
            }
            index++;
        }
        if (index == text.length() - 1 && text.charAt(index) == ' ' || text.charAt(index) == '\n' || text.charAt(index) == '\t' || text.charAt(index) == '<') {
            pair.setLastIndex(index);
            return pair;
        }
        beginOfWord = index;
        while (index < text.length() && text.charAt(index) != ' ' && text.charAt(index) != '\n' && text.charAt(index) != '\t') {
            index++;
        }
        pair.setText(text.substring(beginOfWord, index));
        pair.setLastIndex(index);
        return pair;
    }
    
    @Override
    public Map<String, Long> getWordCounts() throws IllegalStateException{
        try {

            Map<String, Long> wordAndNum = new TreeMap();
            String word;
            long num = 0L;

            String textInLowerCase = this.getText().toLowerCase();
            for (int i = 0; i < textInLowerCase.length(); i++) {
                word = this.findWord(textInLowerCase, i).getText();
                i = this.findWord(textInLowerCase, i).getLastIndex();
                if (!wordAndNum.containsKey(word) && word != null) {                           
                    num = 1L;
                    for (int j = i; j < textInLowerCase.length(); j++) {
                        if(word.equals(this.findWord(textInLowerCase, j).getText())) {
                            num += 1;
                        }
                        j = this.findWord(textInLowerCase, j).getLastIndex();
                    }
                    wordAndNum.put(word, num);
                }
            }
            return wordAndNum;
        }
        catch (Exception e) {
            throw new IllegalStateException();
        }   
    }
    
    @Override
    public List<Map.Entry<String, Long>> getWordCountsSorted() {
        
        Comparator<Map.Entry<String, Long>> comparator = new Comparator<Map.Entry<String, Long>> () {
            
            @Override
            public int compare(Entry<String, Long> e1, Entry<String, Long> e2) {
                if (e1.getValue().compareTo(e2.getValue()) != 0) {
                    return e2.getValue().compareTo(e1.getValue());
                }
                else {
                    return e1.getKey().compareTo(e2.getKey());
                }
            } 
        };
        return this.<String, Long>sort(getWordCounts(), comparator);
    }

    @Override
    public <K extends Comparable<K>, V extends Comparable<V>> List<Map.Entry<K, V>> sort(Map<K, V> map, Comparator<Map.Entry<K, V>> comparator) {
        
        List<Map.Entry<K, V>> sortedList = new ArrayList<>(map.entrySet());
        
        Collections.sort(sortedList, comparator);
        return sortedList;
    }

    @Override
    public <K, V> void print(List<Map.Entry<K, V>> entryList, PrintStream ps) {
        for (Entry<K, V> e: entryList) {
            ps.println(e.getKey() + " " + e.getValue());           
        }
    }
    
}